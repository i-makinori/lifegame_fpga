#/usr/bin/sh
# this file is copy of 
# [http://cellspe.matrix.jp/zerofpga/icarus.html]

EXE=main

iverilog -o $EXE *.v

if [ $? -eq 0 ]; then
  vvp $EXE
fi
