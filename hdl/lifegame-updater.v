`include "defines.h"


module GET_CELLS_NEXT
  #(parameter _NUM_WORLD_CELLS=32)
   (input CLK,
    input                        CELL_CALC_CALL_PULSE,
    input [15:0]                 row, col, height, width,
    input [_NUM_WORLD_CELLS-1:0] WORLD_CELLS_CURRENT,
    //
    output wire                  CELL_CALC_BUSY_P,
    output wire                  living_p_next);
   
   wire [15:0]                   nei_i_list[8:0]; // neighbours_index_list
   reg [3:0]                     nei_sum; // neighbours_sum
   reg                           this_cell_old;
   reg                           this_cell_is_living_p;
   
   reg                           cell_calc_busy_p;
   initial cell_calc_busy_p = 0;
   
   reg [4:0]                     iter; // iter for count neighbour indexes.

   
   twod_torusic_index_with_dd torusic_idnex_0(row, col, height, width, 0, 1, 0, 1, nei_i_list[0]);
   twod_torusic_index_with_dd torusic_idnex_1(row, col, height, width, 0, 1, 0, 0, nei_i_list[1]);
   twod_torusic_index_with_dd torusic_idnex_2(row, col, height, width, 0, 1, 1, 0, nei_i_list[2]);
   
   twod_torusic_index_with_dd torusic_idnex_3(row, col, height, width, 0, 0, 0, 1, nei_i_list[3]);
   twod_torusic_index_with_dd torusic_idnex_4(row, col, height, width, 0, 0, 0, 0, nei_i_list[4]);
   twod_torusic_index_with_dd torusic_idnex_5(row, col, height, width, 0, 0, 1, 0, nei_i_list[5]);
   
   twod_torusic_index_with_dd torusic_idnex_6(row, col, height, width, 1, 0, 0, 1, nei_i_list[6]);
   twod_torusic_index_with_dd torusic_idnex_7(row, col, height, width, 1, 0, 0, 0, nei_i_list[7]);
   twod_torusic_index_with_dd torusic_idnex_8(row, col, height, width, 1, 0, 1, 0, nei_i_list[8]);

   
   always @(posedge CLK or posedge CELL_CALC_CALL_PULSE) begin
      if (CELL_CALC_CALL_PULSE) begin // as like as reset signal
         // just positived
         cell_calc_busy_p <= 1;
         //
         iter <= 0;
         this_cell_is_living_p <= 0;
         nei_sum <= 0;
      end
      else if (CLK && CELL_CALC_BUSY_P) begin // calc ing
         if (iter == 4) begin
            this_cell_old <= (WORLD_CELLS_CURRENT[nei_i_list[4]]);
            iter <= iter+1;
         end
         else if (iter>= 0 && iter<=9-1 && iter!=4) begin
            nei_sum <= nei_sum + (WORLD_CELLS_CURRENT[nei_i_list[iter]]);
            iter <= iter+1;
         end
         else if (iter>9-1) begin
            this_cell_is_living_p
              <= ((this_cell_old  && (nei_sum == 2 || nei_sum == 3)) ||
                  (~this_cell_old && (nei_sum == 3)))
                ? 1 : 0;
            cell_calc_busy_p <= 0;
            iter <= 0;
         end
         else begin
            // something wrong here
            cell_calc_busy_p <= 0;
         end
      end
   end // always @ (posedge CLK)
   
   assign CELL_CALC_BUSY_P = cell_calc_busy_p;
   assign living_p_next = this_cell_is_living_p;
endmodule // GET_CELLS_NEXT   


module twod_torusic_index_with_dd
  (
   input [15:0]      row,
   input [15:0]      col, 
   input [15:0]      hei,
   input [15:0]      wid,
   // row, col, height, width
   input             plus_row_p, minus_row_p, plus_col_p, minus_col_p,
   output wire [15:0] index);
   //
   wire [15:0]       row_index;
   wire [15:0]       col_index;
   
   torusic_index_with_d torus_row(CLK, row, hei, plus_row_p, minus_row_p, row_index);
   torusic_index_with_d torus_col(CLK, col, wid, plus_col_p, minus_col_p, col_index);
   
   assign index = (row_index)*wid + col_index;
endmodule   

module torusic_index_with_d
  (
   input             CLK,
   input [15:0]      index,
   input [15:0]      length,
   input             plus_p, minus_p,
   output wire [15:0] torus_index
   );

   assign torus_index = ((index<=0)        && (plus_p==0) && (minus_p==1)) ? length-1:
                        ((index>=length-1) && (plus_p==1) && (minus_p==0)) ? 0:
                        ((index+plus_p)-minus_p);
endmodule

`ifndef __UPDATER__STATES__

 `define __UPDATER__STATES__

 `define UPDATION_COMPLETED   4'b1000
 `define PARENT_NEXT_UPDATING 4'b0100
 `define CELL_UPDATING        4'b0010

`endif

module LIFEGAME_UPDATER
  #(parameter _NUM_WORLD_CELLS=32)
   (input CLK, input RST,
    //
    input                             UPDATION_PULSE,
    input [15:0]                      CURRENT_GENERATION,
    //
    input [15:0]                      WORLD_HEIGHT, WORLD_WIDTH,
    output reg [_NUM_WORLD_CELLS-1:0] WORLD_CELLS_CURRENT,
    //
    output                            dled4,
    output                            dled3,
    output                            dled2,
    output                            dled1
    );

   /*
   assign dled4 = cell_updated_p;
   assign dled3 = CELL_CALC_JUST_ENDED_P;
   assign dled2 = CELL_CALC_BUSY_P;
   assign dled1 = CELL_CALC_CALL_PULSE;
   */
   assign {dled4, dled3, dled2, dled1} = statement;

   reg [_NUM_WORLD_CELLS-1:0]         world_cells_next;
   
   // ALL RESET THE WORLD
   
   //

   reg [3:0]                          statement;
   initial statement <= `UPDATION_COMPLETED;
   
   reg [15:0]                         i;
   reg [15:0]                         row;
   reg [15:0]                         col;

   wire                               this_cell_is_living_p;

   reg                                CELL_CALC_CALL_PULSE;
   wire                               CELL_CALC_BUSY_P;
   reg                                cell_calc_just_ended;
   reg                                CELL_CALC_JUST_ENDED_P;                               
   reg                                cell_updated_p;
   
   GET_CELLS_NEXT
     #(._NUM_WORLD_CELLS(_NUM_WORLD_CELLS))
   cells_next 
     (.CLK(CLK), .CELL_CALC_CALL_PULSE(CELL_CALC_CALL_PULSE),
      .row(row), .col(col), .height(WORLD_HEIGHT), .width(WORLD_WIDTH), 
      .WORLD_CELLS_CURRENT(WORLD_CELLS_CURRENT), 
      .CELL_CALC_BUSY_P(CELL_CALC_BUSY_P), .living_p_next(this_cell_is_living_p));

   always @(negedge CELL_CALC_BUSY_P or posedge cell_updated_p) begin
      if (cell_updated_p) begin
         CELL_CALC_JUST_ENDED_P <= 0;
      end
      else if (~CELL_CALC_BUSY_P) begin
         CELL_CALC_JUST_ENDED_P <= 1;
      end
   end
   
   always @(posedge CLK or posedge RST or posedge UPDATION_PULSE) begin
      if (RST) begin
         // ALL RESET THE WORLD
         // statement
         statement[3:0] <= `PARENT_NEXT_UPDATING;
         //
         renew_world_cells();      
         //
         i <= 0;
         row <= 0;
         col <= 0;
         CELL_CALC_CALL_PULSE <= 0;
         //
      end // if (RST)
      else if (UPDATION_PULSE) begin
         // statement
         if (statement & `UPDATION_COMPLETED) begin
            // statement
            statement[3:0] <= `CELL_UPDATING;
         end // if (UPDATION_PULSE)
         else begin
         end
      end
      else if (statement & `UPDATION_COMPLETED) begin
         // statement
         statement[3:0] <= `UPDATION_COMPLETED;
         //
         i <= 0;
         row <= 0; col <= 0;
      end
      else if (statement & `PARENT_NEXT_UPDATING) begin
         // refresh screen
         if (i>=_NUM_WORLD_CELLS) begin
            // statement
            statement[3:0] <= `UPDATION_COMPLETED;
            //
            i<=0;
         end
         else begin // index is in board
            WORLD_CELLS_CURRENT[i] <= world_cells_next[i];
            i <= i+1;
         end
      end
      else if (statement & `CELL_UPDATING) begin
         // update cells
         if (i>=_NUM_WORLD_CELLS) begin
            // statement
            statement <= `PARENT_NEXT_UPDATING;
            //
            CELL_CALC_CALL_PULSE <= 0;
            cell_updated_p <= 0;
            //
            i <= 0;
            row <= 0; col <= 0;
         end
         else begin
            if (cell_updated_p) begin
               i <= i+1;
               if (col>=WORLD_WIDTH-1) begin
                  row <= row+1; col <= 0;
               end
               else begin
                  row <= row; col <= col+1;
               end
               // cellcalc statement
               cell_updated_p <= 0; CELL_CALC_CALL_PULSE <= 0;
            end
            else if (CELL_CALC_JUST_ENDED_P) begin
               world_cells_next[i] <= this_cell_is_living_p;
               // cell calc statement
               cell_calc_just_ended <= 0; cell_updated_p <= 1;
            end
            else if (CELL_CALC_BUSY_P==1 && CELL_CALC_CALL_PULSE==1) begin
               // cell_calc_ing
               CELL_CALC_CALL_PULSE <= 0;
            end
            else if (CELL_CALC_BUSY_P==0 && CELL_CALC_CALL_PULSE == 0) begin
               // call next cell_calc
               CELL_CALC_CALL_PULSE <= 1;
            end
         end
      end // if (statement & `CELL_UPDATING)
      else begin
         CELL_CALC_CALL_PULSE <= 1;
      end
   end

   
   task renew_world_cells;
      //reg [_NUM_WORLD_CELLS-1:0] wirld_cells_next;
      begin
     
         world_cells_next[15:13] <= `glider_0;
         world_cells_next[27:25] <= `glider_1;
         world_cells_next[39:37] <= `glider_2;
         
         //world_cells_next[72+5+3:72+3] <= `spaceship_0;
         //world_cells_next[84+5+3:84+3] <= `spaceship_1;
         //world_cells_next[0+5+3:0+3] <= `spaceship_2;
         //world_cells_next[12+5+3:12+3] <= `spaceship_3;

         // world_cells_next[17+5:13+5] <= `spaceship_0;
         // world_cells_next[29+5:25+5] <= `spaceship_1;
         // world_cells_next[41+5:37+5] <= `spaceship_2;
         // world_cells_next[53+5:49+5] <= `spaceship_3;
      end 
   endtask
   
endmodule // LIFEGAME_UPDATER

`ifdef __UPDATER__STATES__

 `undef __UPDATER__STATES__

 `undef UPDATION_COMPLETED   
 `undef PARENT_NEXT_UPDATING 
 `undef CELL_UPDATING        

`endif


