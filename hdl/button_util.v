
module ChatteringTaktButtonSignal
  // Takt Button's Just_Pressed Detection which chattering is removed.
  // Note: This Does not Detect Pressing.
  (input CLK, RST,
   input      BUTTON_IN,
   output reg out_signal
   );
   
   reg [20:0] hlz_counter;
   wire       en40hz = (hlz_counter == 1250000 - 1); // every 25[ms].

   always @(posedge CLK or posedge RST) begin
      if (RST)
        hlz_counter = 21'b0;
      else if (en40hz)
        hlz_counter = 21'b0;
      else
        hlz_counter = hlz_counter + 21'b1;
   end

   reg ff1, ff2;

   always @(posedge CLK or posedge RST) begin
      if (RST) begin
         ff2 <= 1'b0;
         ff1 <= 1'b0;
      end
      else if (en40hz) begin
         ff2 <= ff1;
         ff1 <= BUTTON_IN;
      end
   end

   wire temp = ~ff1 & ff2 & en40hz;

   always @(posedge CLK, posedge RST) begin
      if (RST)
        out_signal <= 0;
      else
        out_signal <= temp;
   end
   
endmodule


module ChatteringToggleButtonSignal
  // Toggle Button State Detection which chattering is removed.
  (input CLK, 
   input      RST,
   input      BUTTON,
   output reg which_signal);
   
endmodule
