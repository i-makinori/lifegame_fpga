// Referenced
// [VGA_1](http://tinyvga.com/vga-timing/640x480@60Hz) # for timing
// [VGA_2](http://reis05.s602.xrea.com/161vga.html) # for imagine
// [VGA_3](https://tomverbeure.github.io/video_timings_calculator) # for calculate

`include "defines.h"

module LIFEGAME_VGA
  #(parameter _NUM_WORLD_CELLS=32)
   (//
    input                        CLK,
    input                        RST,
    // RGB pins
    output reg [3:0]             VGA_R, VGA_G, VGA_B,
    // vertical sync and holizonal sync 
    output wire                  VGA_VS, VGA_HS,
    //
    input                        UPDATION_PULSE,
    input [15:0]                 CURRENT_GENERATION,
    // Lifegame params
    input [15:0]                 WORLD_HEIGHT,
    input [15:0]                 WORLD_WIDTH,
    input [_NUM_WORLD_CELLS-1:0] WORLD_CELLS_CURRENT
    );


   //
   parameter        _H_VISIBLE = 640;
   parameter        _V_VISIBLE = 480;
   
   
   // pixel_clk
   wire             pixel_clk;
   wire             visible_p;
   wire [9:0]       h_visible_pix; // 0 to (640-1)
   wire [9:0]       v_visible_pix; // 0 to (480-1)

   // current board index
   reg [15:0]       world_row;
   reg [15:0]       world_col;
   wire             world_address_exist_p;
   wire [15:0]      world_address;       
   
   reg              world_cell_current;   
   
   // VGA Signals
   VGA_640x480at60hlz_Signals vga_signals
     (.CLK(CLK), .RST(RST),
      //
      .PIXEL_CLOCK(pixel_clk),
      .VGA_VS(VGA_VS), .VGA_HS(VGA_HS),
      //
      .VISIBLE_P(visible_p),
      .H_VISIBLE_PIX(h_visible_pix),
      .V_VISIBLE_PIX(v_visible_pix)
      //
      );

   
   // show VGA
   parameter        _vga_mode = 1;
   parameter        _grid = 8;
   

   always @(posedge pixel_clk) begin
      //if (h_visible_p && v_visible_p) begin
      if (~visible_p) begin
         {VGA_R, VGA_G, VGA_B} <= 12'b0000_0000_0000;
      end
      else begin 
         if (_vga_mode == 0) begin
            VGA_R = 4'b0000;
            VGA_G[3:0] = (CURRENT_GENERATION%16);
            //VGA_G[1:0] = 2'b00;
            VGA_B = 4'b1000;
         end
         else if (_vga_mode == 1) begin
            world_row[9:0] = v_visible_pix / _grid; // % 32
            world_col[9:0] = h_visible_pix / _grid; // % 32
            
            // world address is assigned
            if(`row_col_is_exist_in_world(world_row, world_col, WORLD_HEIGHT, WORLD_WIDTH)) begin
               VGA_R = 4'b0000;
               VGA_G = (WORLD_CELLS_CURRENT
                        [`row_col_to_world_index(world_row, world_col, WORLD_HEIGHT, WORLD_WIDTH)])
                 ? 4'b1110 : 4'b0000;
               VGA_B = 4'b0000; //(CURRENT_GENERATION%16);
               if(h_visible_pix%_grid==0 || v_visible_pix%_grid==0) begin
                  {VGA_R, VGA_G, VGA_B} = {VGA_R, VGA_G, VGA_B} + 12'b0011_0011_0011;
               end
            end
            else begin
               {VGA_R, VGA_G, VGA_B} = 12'b0011_0111_0111;
            end
         end
      end
   end
endmodule



module VGA
  (//
   input            CLK,
   input            RST,
   // RGB pins
   output reg [3:0] VGA_R, VGA_G, VGA_B,
   // vertical sync and holizonal sync 
   output wire      VGA_VS, VGA_HS   
);


   //
   parameter        _H_VISIBLE = 640;
   parameter        _V_VISIBLE = 480;
   
   
   // pixel_clk
   wire             pixel_clk;
   wire             visible_p;
   wire [9:0]       h_visible_pix; // 0 to (640-1)
   wire [9:0]       v_visible_pix; // 0 to (480-1)
   
   // VGA Signals
   VGA_640x480at60hlz_Signals vga_signals
     (.CLK(CLK), .RST(RST),
      //
      .PIXEL_CLOCK(pixel_clk),
      .VGA_VS(VGA_VS), .VGA_HS(VGA_HS),
      //
      .VISIBLE_P(visible_p),
      .H_VISIBLE_PIX(h_visible_pix),
      .V_VISIBLE_PIX(v_visible_pix)
      //
      );

   
   // show VGA
   parameter        _vga_mode = 2;

   always @(posedge pixel_clk) begin
      //if (h_visible_p && v_visible_p) begin
      if (~visible_p) begin
         {VGA_R, VGA_G, VGA_B} <= 12'b0000_0000_0000;
      end
      else begin 
         if (_vga_mode == 0) begin
            {VGA_R, VGA_G, VGA_B} <= 12'b0110_1111_1001;
         end
         if (_vga_mode == 1) begin
            VGA_R <= (h_visible_pix%16 < 8) 
              ? 4'b0100 : 4'b0000 ;
            VGA_G <= ((h_visible_pix%16 < 8) || (v_visible_pix%16 < 8))
              ? 4'b1111 : 4'b0000 ;
            VGA_B <= (h_visible_pix%16 < 8) 
              ? 4'b0000 : 4'b1000 ;
         end
         if (_vga_mode == 2) begin
            VGA_R <= (h_visible_pix%5'b10000);
            VGA_G <= 4'b0000;
            VGA_B <= (v_visible_pix%5'b10000);
         end
      end
   end
endmodule


module VGA_640x480at60hlz_Signals
  (input CLK,
   input            RST,
   //
   output reg       PIXEL_CLOCK,
   output reg       VGA_VS, VGA_HS,
   //
   output reg       VISIBLE_P,
   output reg [9:0] H_VISIBLE_PIX,
   output reg [9:0] V_VISIBLE_PIX
   );

   reg [9:0]        h_line_current; // 0 to (800-1)
   reg [9:0]        v_line_current; // 0 to (525-1)
   
   reg              h_visible_p;
   reg              v_visible_p;


   
   // parameters of 640x480@60[HlZ] VGA Signals
   // _H_* => pixels
   parameter        _H_BACK_PORCH = 48;
   parameter        _H_VISIBLE = 640;
   parameter        _H_FRONT_PORCH = 16;
   parameter        _H_SYNC = 96;
   parameter        _H_WHOLE_LINE = _H_BACK_PORCH + _H_VISIBLE + _H_FRONT_PORCH + _H_SYNC; // = 800
   // _V_* => lines
   parameter        _V_BACK_PORCH = 33;
   parameter        _V_VISIBLE = 480;
   parameter        _V_FRONT_PORCH = 10;
   parameter        _V_SYNC = 2;
   parameter        _V_WHOLE_FRAME = _V_BACK_PORCH + _V_VISIBLE + _V_FRONT_PORCH + _V_SYNC; // = 525
   

   // where CLK is 50MHlz in DE0.
   // reg        PIXEL_CLOCK ;    // pixel cock is 25MHlz,
   always @(posedge CLK or posedge RST) begin
      if (RST) begin
         PIXEL_CLOCK <= 0;
      end
      else begin
         PIXEL_CLOCK <= ~PIXEL_CLOCK;
         VISIBLE_P <= v_visible_p && h_visible_p;
      end
   end
   
   // Horizontal Signals
   always @(posedge PIXEL_CLOCK or posedge RST) begin
      if (RST) begin
         h_line_current <= 0;
      end

      // Horizontal_Syncs and h_visible_p
      else if (PIXEL_CLOCK) begin 
         case(h_line_current)
           0   :          // back porch    
             VGA_HS <= 0; 
           (_H_BACK_PORCH-1) : begin // visible area
              VGA_HS <= 0; 
              h_visible_p <= 1; // v is visible area
           end
           (_H_BACK_PORCH+_H_VISIBLE-1) : begin // front porch
              VGA_HS <= 0; 
              h_visible_p <= 0; // v is not visible area
           end
           (_H_BACK_PORCH+_H_VISIBLE+_H_FRONT_PORCH-1): // H Sync
             VGA_HS <= 1;
           default: begin // in another 
              VGA_HS <= VGA_HS;
              h_visible_p <= h_visible_p;
           end
         endcase // case (h_line_current)

         // H_VISIBLE_PIX
         H_VISIBLE_PIX <= (~h_visible_p)
           ? 10'b0 : H_VISIBLE_PIX+10'b1;
         
         // next line
         h_line_current <= (h_line_current==_H_WHOLE_LINE-1)
           ? 10'b0 : h_line_current+10'b1;
      end
   end
   
   // Vertical Signals
   always @(posedge VGA_HS or posedge RST) begin
      if (RST) begin
         v_line_current <= 0;
      end
      
      // Vertical_Syncs and v_visible_p
      else if (VGA_HS) begin
         case(v_line_current)
           0   :          // back porch    
             VGA_VS <= 0; 
           (_V_BACK_PORCH-1) : begin // visible area
              VGA_VS <= 0; 
              v_visible_p <= 1; // v is visible area
           end
           (_V_BACK_PORCH+_V_VISIBLE-1) : begin // front porch
              VGA_VS <= 0; 
              v_visible_p <= 0; // v is not visible area
           end
           (_V_BACK_PORCH+_V_VISIBLE+_V_FRONT_PORCH-1): // V Sync
             VGA_VS <= 1; 
           default: begin
              VGA_VS <= VGA_VS;
              v_visible_p <= v_visible_p;
           end
         endcase // case (h_line_current)

         // V_VISIBLE_PIX
         V_VISIBLE_PIX <= (~v_visible_p)
           ? 10'b0 : V_VISIBLE_PIX+10'b1;
         
         // next frame
         v_line_current <= (v_line_current==_V_WHOLE_FRAME-1)
           ? 10'b0 : v_line_current+10'b1;
      end
   end
endmodule  

