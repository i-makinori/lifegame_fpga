
module ClockUtil
  (input wire CLK,
   input wire        RST,
   //
   input             MODE_SW,
   input             GENERATIONS_BUTTON,
   output [9:0]      TEST_LED,
   input [3:0]       HLZ_SW,
   //
   output reg        EN_1HZ,
   output reg        EN_EXAM_HZ,
   output reg        EN_GENERATION,
   output reg [15:0] GENERATION_COUNTER,
   //
   output [6:0]      HEX0, HEX1, HEX2, HEX3
   );


   parameter    _MODE_SECOND = 0;
   parameter    _MODE_BUTTON = 1;
   
   wire         en_1hz;
   wire         en_10hz;
   wire         en_100hz;
   wire         en_1000hz;
   wire         en_megahz;
   
   wire         en_exam_hz;         

   reg          generation_updated_p;
   //wire         en_generation;
   

   

   OneHlzSignal one_hlz_signal
     (.CLK(CLK), .RST(RST), .EN_1HZ(en_1hz));

   ExamHlzSignal
     #(.N_POSITIVE(26'd4999999))
   ten_hlz_signal
     (.CLK(CLK), .RST(RST), .EN_EXAM_HZ(en_10hz));

   ExamHlzSignal
     #(.N_POSITIVE(26'd499999))
   hund_hlz_signal
     (.CLK(CLK), .RST(RST), .EN_EXAM_HZ(en_100hz));

   ExamHlzSignal
     #(.N_POSITIVE(26'd49999))
   kilo_hlz_signal
     (.CLK(CLK), .RST(RST), .EN_EXAM_HZ(en_1000hz));

   ExamHlzSignal
     #(.N_POSITIVE(26'd49))
   mega_hlz_signal
     (.CLK(CLK), .RST(RST), .EN_EXAM_HZ(en_megahz));
   
   //ExamHlzSignal exam_hlz_signal
   //(.CLK(CLK), .RST(RST), .EN_EXAM_HZ(en_exam_hz));

   assign en_exam_hz = (HLZ_SW[3]) ? en_1hz:
                       (HLZ_SW[2]) ? en_10hz:
                       (HLZ_SW[1]) ? en_100hz:
                       (HLZ_SW[0]) ? en_1000hz:
                       en_megahz;
   
   always @(posedge CLK or posedge RST) begin
      
      generation_updated_p = 0;
      
      if (RST) begin
         GENERATION_COUNTER = 16'b0;
      end
      else begin
         if (MODE_SW==_MODE_SECOND && en_1hz) begin
            GENERATION_COUNTER = GENERATION_COUNTER + 16'b1;
            generation_updated_p = 1;
         end
         //
         else if (MODE_SW==_MODE_BUTTON && GENERATIONS_BUTTON) begin
            GENERATION_COUNTER = GENERATION_COUNTER + 16'b1;
            generation_updated_p = 1;
         end

         EN_1HZ = en_1hz;
         EN_GENERATION = generation_updated_p;
         EN_EXAM_HZ = en_exam_hz;
      end // else: !if(RST)
   end // always @ (posedge CLK or posedge RST)



   
   Show16bitValue show_generation
     (.value16bit(GENERATION_COUNTER[15:0]),
      .HEX0(HEX0[6:0]), .HEX1(HEX1[6:0]), .HEX2(HEX2[6:0]), .HEX3(HEX3[6:0]));

   assign TEST_LED[0] = RST;
   assign TEST_LED[1] = MODE_SW;
   assign TEST_LED[2] = GENERATIONS_BUTTON;
   
endmodule // ClockUtil

module ExamHlzSignal // Hundred Hlz
  #(parameter N_POSITIVE=26'd49999999) // = 1 sec
  (input wire  CLK,
   input wire  RST,
   output wire EN_EXAM_HZ);
   
   reg [25:0]  exam_sec_counter;

   always @(posedge CLK, posedge RST) begin
      if (RST)
        exam_sec_counter <= 26'b0;
      else if (EN_EXAM_HZ)
        exam_sec_counter <= 26'b0;
      else
        exam_sec_counter <= 26'b1 + exam_sec_counter;
   end

   assign EN_EXAM_HZ = (exam_sec_counter==N_POSITIVE); // 100 hlz
   // system clock is 50MHlz.
   
endmodule // OneHlzSignal


module OneHlzSignal
  (input wire  CLK,
   input wire  RST,
   output wire EN_1HZ);

   
   reg [25:0]  one_sec_counter;

   always @(posedge CLK, posedge RST) begin
      if (RST)
        one_sec_counter <= 26'b0;
      else if (EN_1HZ)
        one_sec_counter <= 26'b0;
      else
        one_sec_counter <= 26'b1 + one_sec_counter;
   end

   assign EN_1HZ = (one_sec_counter==26'd49999999); // system clock is 50MHlz.
   
endmodule // OneHlzSignal
