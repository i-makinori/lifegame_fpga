`ifndef __DEFINES_HEADER__

 `define __DEFINES_HEADER__
//

// row and col to world array index
 `define row_col_to_world_index(row, col, height, width) (row*width+col)

// does row and col exist in world array?
 `define row_col_is_exist_in_world(row, col, height, width) \
(row>=0 && row<height && col>=0 && col<width)


//
 `define glider_0 3'b111
 `define glider_1 3'b100
 `define glider_2 3'b010

 `define spaceship_0 5'b10010
 `define spaceship_1 5'b00001
 `define spaceship_2 5'b10001
 `define spaceship_3 5'b01111

`endif
