
module Show16bitValue
  (input [15:0] value16bit,
   output [6:0] HEX0,
   output [6:0] HEX1,
   output [6:0] HEX2,
   output [6:0] HEX3);
   
  
   SevenSegDisplay seg_digit_0
     (.value(value16bit[3:0]),
      .HEXs(HEX0[6:0]));
   SevenSegDisplay seg_digit_1
     (.value(value16bit[7:4]),
      .HEXs(HEX1[6:0]));
   SevenSegDisplay seg_digit_2
     (.value(value16bit[11:8]),
      .HEXs(HEX2[6:0]));
   SevenSegDisplay seg_digit_3
     (.value(value16bit[15:12]),
      .HEXs(HEX3[6:0]));
   
endmodule // Show16bitValue


module SevenSegDisplay
  (input [3:0] value,
   output reg [6:0] HEXs);
   
   always @* begin
      case (value)
        4'h0: HEXs = 7'b1000000;
        4'h1: HEXs = 7'b1111001;
        4'h2: HEXs = 7'b0100100;
        4'h3: HEXs = 7'b0110000;
        4'h4: HEXs = 7'b0011001;
        4'h5: HEXs = 7'b0010010;
        4'h6: HEXs = 7'b0000010;
        4'h7: HEXs = 7'b1011000;
        4'h8: HEXs = 7'b0000000;
        4'h9: HEXs = 7'b0010000;
        4'ha: HEXs = 7'b0001000;
        4'hb: HEXs = 7'b0000011;
        4'hc: HEXs = 7'b1000110;
        4'hd: HEXs = 7'b0100001;
        4'he: HEXs = 7'b0000110;
        4'hf: HEXs = 7'b0001110;
        default: HEXs = 7'bxxxxxxx;
      endcase 
   end
endmodule // SevenSegDisplay

