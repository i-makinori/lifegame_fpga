`include "defines.h"

module LifegameFPGA
  (
   // Control Signal
   input        CLOCK_50,
   // input        RST, is BUTTON[0]
   // User Interface I/O
   input [9:0]  SW,
   input [2:0]  BUTTON, 
   output [9:0] LEDG,
   // Show status
   output [6:0] HEX0_D, HEX1_D, HEX2_D, HEX3_D,
   // VGAs
   output [3:0] VGA_R, VGA_G, VGA_B,
   output       VGA_VS, VGA_HS
   // something next
   );

   wire         EXAM_HLZ_PULSE;
   

   // Wire and Registers
   // Control Signal
   wire         RST;
   wire         CLK_50M;
   // Updation Signal
   wire         ONE_HLZ_PULSE; // 1 Hlz
   wire         BUTTON_UPDATION_PULSE; // User Input for Updation
   wire         GENERATION_UPDATE_PULSE; // Array Updator Catches This Pulse. then, update Array.
   wire [15:0]  GENERATION_COUNTER;
   //

   // World Constant
   parameter    _WORLD_HEIGHT = 8; //12 //24;
   parameter    _WORLD_WIDTH = 12; //16 //32;
   parameter    _NUM_WORLD_CELLS =  _WORLD_HEIGHT * _WORLD_WIDTH;
   
   // reg [0]      WORLD_CELLS_ARRAY [_NUM_WORLD_CELLS-1:0];
   wire [_NUM_WORLD_CELLS-1:0] WORLD_CELLS_CURRENT;
   
   
   // Control Signal
   assign CLK_50M = CLOCK_50;
   
   /*
    // selfly resseting 
    ChatteringTaktButtonSignal reset_signal
    (.CLK(CLK_50), .RST(~BUTTON[0]), .BUTTON_IN(BUTTON[0]), .out_signal(RST));
   */
   assign RST = ~BUTTON[0]; // unsefely resetting now
   
   // User Interface I/O
   ChatteringTaktButtonSignal clock_button_signal
     (.CLK(CLK_50M), .RST(RST),
      .BUTTON_IN(BUTTON[2]),
      .out_signal(BUTTON_UPDATION_PULSE));

   wire                        BUTTON_DRAW_PULSE; // User Input for Updation

   assign BUTTON_DRAW_PULSE = ~BUTTON[1];
   


   // World Clock, and World Clock's UIs
   ClockUtil clocks // update 1hz_clock and generation_clock
     (.CLK(CLK_50M), .RST(RST),
      //
      .MODE_SW(SW[0]),
      .GENERATIONS_BUTTON(BUTTON_UPDATION_PULSE),
      .TEST_LED(LEDG[5:0]),
      .HLZ_SW(SW[9:6]),
      //
      .EN_1HZ(ONE_HLZ_PULSE),
      .EN_EXAM_HZ(EXAM_HLZ_PULSE),
      .EN_GENERATION(GENERATION_UPDATE_PULSE),
      .GENERATION_COUNTER(GENERATION_COUNTER),
      //
      .HEX0(HEX0_D[6:0]), .HEX1(HEX1_D[6:0]), .HEX2(HEX2_D[6:0]), .HEX3(HEX3_D[6:0]));

   // World Array and World Updation
   LIFEGAME_UPDATER
     #(._NUM_WORLD_CELLS(_NUM_WORLD_CELLS))
   lifegame_updater
     (.CLK(EXAM_HLZ_PULSE), //.CLK(CLK_50M), 
      .RST(RST),
      //
      .UPDATION_PULSE(GENERATION_UPDATE_PULSE),
      .CURRENT_GENERATION(GENERATION_COUNTER),
      //
      .WORLD_HEIGHT(_WORLD_HEIGHT),
      .WORLD_WIDTH(_WORLD_WIDTH),
      .WORLD_CELLS_CURRENT(WORLD_CELLS_CURRENT),
      //
      .dled4(LEDG[9]),
      .dled3(LEDG[8]),
      .dled2(LEDG[7]),
      .dled1(LEDG[6])
      );
   
   
   // VGA Output
   /*
   VGA vga
     (.CLK(CLK_50M), .RST(RST),
      .VGA_R(VGA_R), .VGA_G(VGA_G), .VGA_B(VGA_B),
      .VGA_VS(VGA_VS), .VGA_HS(VGA_HS));
   */

   // Lifegame VGA Output
   LIFEGAME_VGA
     #(._NUM_WORLD_CELLS(_NUM_WORLD_CELLS))
   lifegame_vga
     (
      .CLK(CLK_50M), .RST(RST),
      .VGA_R(VGA_R), .VGA_G(VGA_G), .VGA_B(VGA_B),
      .VGA_VS(VGA_VS), .VGA_HS(VGA_HS),
      //
      .UPDATION_PULSE(GENERATION_UPDATE_PULSE),
      .CURRENT_GENERATION(GENERATION_COUNTER),
      //
      .WORLD_HEIGHT(_WORLD_HEIGHT),
      .WORLD_WIDTH(_WORLD_WIDTH),
      .WORLD_CELLS_CURRENT(WORLD_CELLS_CURRENT)
      );

endmodule
