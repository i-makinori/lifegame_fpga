

`timescale 1ns / 1ps
module lifegame_updater_simulation;

   // enviroment waves
   parameter STEP = 20; // 20ns <=> 50Mhlz
   parameter TICKS = 20000; // num clocks to monitor
   reg       CLK;
   reg       RST;

   // what to monitor
   initial begin
      $dumpfile("simu_updator.vcd");
      $dumpvars(0, simu_updater);
      $monitor("index: %d", simu_updater.i);
   end

   // generate clock
   initial begin
      CLK=1'b1;
      forever begin
         #(STEP / 2) CLK = ~CLK;
      end
   end

   // First, Reset Signal
   initial begin
      RST = 1'b0;
      // reset 2 Clocks
      repeat (2) @(posedge CLK) RST<=1'b1;
      @(posedge CLK) RST <= 1'b0;
   end
   
   // End Simulation
   initial begin
      repeat (TICKS) @(posedge CLK);
      $finish;
   end

   // variables

   reg GENERATION_UPDATE_PULSE;
   reg [15:0] generation_counter;

   initial generation_counter <= 0;
   
   always @(posedge CLK) begin      
      if (generation_counter >= 10) begin
         generation_counter <= 0;
         GENERATION_UPDATE_PULSE <= 1;
      end
      else begin
         generation_counter <= generation_counter + 1;
         GENERATION_UPDATE_PULSE <= 0;
      end
   end

   wire LEDG_4;
   wire LEDG_3;
   wire LEDG_2;
   wire LEDG_1;
   
   
   
   parameter _WORLD_HEIGHT = 8;
   parameter _WORLD_WIDTH = 12;
   parameter _NUM_WORLD_CELLS = _WORLD_HEIGHT * _WORLD_WIDTH;

   wire [_NUM_WORLD_CELLS-1:0] WORLD_CELLS_CURRENT;
   
   


   // World Array and World Updation
   LIFEGAME_UPDATER
     #(._NUM_WORLD_CELLS(_NUM_WORLD_CELLS))
   simu_updater
     (.CLK(CLK), //.CLK(CLK_50M), 
      .RST(RST),
      //
      .UPDATION_PULSE(GENERATION_UPDATE_PULSE),
      .CURRENT_GENERATION(GENERATION_COUNTER),
      //
      .WORLD_HEIGHT(_WORLD_HEIGHT),
      .WORLD_WIDTH(_WORLD_WIDTH),
      .WORLD_CELLS_CURRENT(WORLD_CELLS_CURRENT),
      //
      .dled4(LEDG_4),
      .dled3(LEDG_3),
      .dled2(LEDG_2),
      .dled1(LEDG_1));
   
endmodule


`timescale 1ns / 1ps
module torusic_index_simulation;

   // enviroment waves
   parameter STEP = 20; // 20ns <=> 50Mhlz
   parameter TICKS = 200; // num clocks to monitor
   reg       CLK;
   reg       RST;

   // what to monitor
   initial begin
      $dumpfile("simu.vcd");
      $dumpvars(0, simu_torusic);
      $monitor("index: %d", simu_torusic.index);
   end

   // generate clock
   initial begin
      CLK=1'b1;
      forever begin
         #(STEP / 2) CLK = ~CLK;
      end
   end

   // First, Reset Signal
   initial begin
      RST = 1'b0;
      // reset 2 Clocks
      repeat (2) @(posedge CLK) RST<=1'b1;
      @(posedge CLK) RST <= 1'b0;
   end
   
   // End Simulation
   initial begin
      repeat (TICKS) @(posedge CLK);
      $finish;
   end

   // variable signs
   reg [15:0] row;
   reg [15:0] col;
   //wire [15:0] height;
   //wire [15:0] width;
   wire [15:0] index;

   reg [15:0]  iter;
   

   parameter   height = 8;
   parameter   width = 12;

   initial begin
      row<=0;
      col<=0;
   end

   always @(posedge CLK) begin
      iter <= iter+1;   
      if (col>=width-1) begin
         row <= row+1; col <= 0;
      end
      else begin
         row <= row; col <= col+1;
      end
   end
   
   twod_torusic_index_with_dd simu_torusic(row, col, height, width, 0, 1, 0, 1, index);
   
endmodule
